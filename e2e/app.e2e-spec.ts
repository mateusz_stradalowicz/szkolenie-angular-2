import { SzkolenieAngular2Page } from './app.po';

describe('szkolenie-angular2 App', function() {
  let page: SzkolenieAngular2Page;

  beforeEach(() => {
    page = new SzkolenieAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
