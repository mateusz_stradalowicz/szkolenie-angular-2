import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent {

  @Input() user: User;
  @Output() userss: EventEmitter<User[]> = new EventEmitter<User[]>();

  constructor(private userService: UserService) { }

  delete(login: string): void {
    this.userService.delete(login)
      .then(users => {
        this.userss.emit(users);
      });
  }
}
