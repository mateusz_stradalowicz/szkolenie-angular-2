import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [UserService]
})
export class UserListComponent implements OnInit {
  title = 'My users';
  @Input() users: User[];
  selectedUser: User;
  constructor(private userService: UserService) { }
  getHeroes(): void {
    this.userService.getUsers().then(users => this.users = users);
  }
  ngOnInit(): void {
    this.getHeroes();
  }
  onSelect(user: User): void {
    this.selectedUser = user;
  }
  add(login: string, userName: string): void {
    this.userService.create(login, userName)
      .then(users => {
        this.users = users;
        this.selectedUser = null;
      });
  }

  updateUsers(users): void {

  }

  log = (aaa) => {
    console.log(aaa);
  }
}
