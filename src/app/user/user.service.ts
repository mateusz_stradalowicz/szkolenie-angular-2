import { Injectable } from '@angular/core';
import { User } from './user';
import { USERS } from './mock-users';
import * as Enumerable from 'linq';

@Injectable()
export class UserService {
  getUsers(): Promise<User[]> {
    return Promise.resolve(USERS);
  }

  // See the "Take it slow" appendix
  getUsersSlowly(): Promise<User[]> {
    return new Promise(resolve => {
      // Simulate server latency with 2 second delay
      setTimeout(() => resolve(this.getUsers()), 2000);
    });
  }
  create(login: string, userName: string): Promise<User[]> {
    USERS.push({login, userName});
     return Promise.resolve(USERS);
  }
  delete(login: string): Promise<User[]> {
    const index = Enumerable.from(USERS).where(u => u.login === login).firstOrDefault();
    USERS.splice(USERS.indexOf(index), 1);
    return Promise.resolve(USERS);
  }
}
