import { User } from './user';

export const USERS: User[] = [
    {login: 'BadBoy7', userName: 'Jan Kowalski'},
    {login: 'MisterImperio44', userName: 'Daniel Wróbel'},
    {login: 'Majster', userName: 'Borys Kucharski'},
    {login: 'JestemBogiem555', userName: 'Igor Nowicki'},
    {login: 'LubiePlacki', userName: 'Aleksandra Sobczyk'},
];
