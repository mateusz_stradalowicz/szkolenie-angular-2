export class Book {
  title: string;
  author: string;
  identificationNumber: string;
}
